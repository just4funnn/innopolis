package ru.shevtsov.lesson3.task2;

import java.util.Scanner;

public class HotCold {

    public static void main(String[] args) {

        int min = 0;
        int max = 100;
        int lastone = -1;
        int num;

        int randomNumber = min + (int) (Math.random() * max); //Задали рандомное число от 0 до 100

        do {

            Scanner in = new Scanner(System.in);
            System.out.println("Введите любое число"); //Вводим любое число от пользователя
            num = in.nextInt();

            int distanceToNum = Math.abs(randomNumber - num);
            int lastDistance = Math.abs(randomNumber - lastone);

            if (distanceToNum > lastDistance) {
                System.out.println("холодно");
            } else {
                System.out.println("горячо");
            }
            lastone = num;

        } while (randomNumber != num);
    }

}
